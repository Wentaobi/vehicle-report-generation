import mlreportgen.report.*
import mlreportgen.dom.*

rpt = Report('My Report','html');
add(rpt,TitlePage('Title','My Report'));
add(rpt,TableOfContents);
chTitle = Heading1('Chapter ');
chTitle.Style = {CounterInc('sect1'),...
     WhiteSpace('preserve')...
     Color('black'),...
     Bold, FontSize('24pt')};
append(chTitle,AutoNumber('sect1'));
append(chTitle,'. ');

sectTitle = Heading2();
sectTitle.Style = {CounterInc('sect2'),...
     WhiteSpace('preserve') ...
     HAlign('center'),PageBreakBefore};
append(sectTitle,AutoNumber('sect1'));
append(sectTitle,'.');
append(sectTitle,AutoNumber('sect2'));
append(sectTitle,'. ');
title = clone(chTitle);
append(title,'Images');
ch = Chapter('Title',title);
title = clone(sectTitle());
append(title,'Boeing 747');
add(ch,Section('Title',title,'Content',...
     Image(which('b747.jpg'))));
title = clone(sectTitle());
append(title,'Peppers');
add(ch,Section('Title',title,'Content',Image(which('peppers.png'))));

add(rpt,ch);
close(rpt);
rptview(rpt);